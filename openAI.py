import gym
import random
import numpy as np  
import tflearn
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
from statistics import mean,median
from collections import Counter
#for bullshit windows 10 problems
gym.logger.set_level(40)
#setting up learning rate
LR = 1e-3
#making environment

env = gym.make('CartPole-v1')
env.reset()
goal_steps = 500
score_requirements = 50
initial_games = 10000

#defining games
# =============================================================================
# def random_games():
#     for episode in range(5):
#         env.reset()
#         for t in range(200):
#             #env.render()
#             #not rendering because it will use up resources
#             action = env.action_space.sample()
#             observation, reward, done, info = env.step(action)
#             if done:
#                 break
# =============================================================================

#testing random games code            
#random_games()

#defining our populations
def initial_populations():
    #setting up blank lists to insert data in 
    training_data = []
    
    scores = []
    
    accepted_scores = []
    
    for _ in range(initial_games):
        #code for 10000 iters of games
        score = 0
        
        game_memory = []
        
        prev_obs =  []
        
        for _ in range(goal_steps):
            
            action = random.randrange(0,2)
            
            observation, reward, done, info = env.step(action)
            
            if len(prev_obs) > 0:                
                game_memory.append([prev_obs, action])
            prev_obs = observation
            score += reward
                
            if done: break
            
        if score >= score_requirements:
            accepted_scores.append(score)
            #making output data onehot
            for data in game_memory:
                
                if data[1] == 1:
                    
                    output = [0,1]
                    
                elif data [1] == 0:
                    
                    output = [1,0]
                    
                training_data.append([data[0], output])
                    
        #resetting game
        env.reset()
        
        scores.append(score)
    
    #saving out training data     
    training_data_save = np.array(training_data)
    
    np.save("saved_training_data.npy", training_data_save)
    
    print("Average accepted score:", mean(accepted_scores))
    print("Median accepted score:", median(accepted_scores))
    print(Counter(accepted_scores))
    
    return training_data

#testing out function
initial_populations()
                

#creating neural network model using tflearn


def neural_network_model(input_size):
    network = input_data(shape = [None,input_size, 1], name = 'input')
    
    #making out fully connected layers
    #1st layer
    network = fully_connected(network, 128, activation='relu')
    #1st layer dropout
    network = dropout(network,0.8)
    #2nd layer
    network = fully_connected(network, 256, activation='relu')
    #2nd layer dropout
    network = dropout(network,0.8)
    #3rd layer
    network = fully_connected(network, 512, activation='relu')
    #3rd layer dropout
    network = dropout(network,0.8)
    #4th layer
    network = fully_connected(network, 256, activation='relu')
    #4th layer dropout
    network = dropout(network,0.8)
    #5th layer
    network = fully_connected(network, 128, activation='relu')
    #5th layer dropout
    network = dropout(network,0.8)
    #output layer
    network = fully_connected(network,2,activation='softmax')
    #adding regression for loss functions
    network = regression(network,optimizer='adam', learning_rate=LR, loss='categorical_crossentropy', name='targets')
    
    model = tflearn.DNN(network, tensorboard_dir="log")
    
    return model

def train_model(training_data,model=False):
    X = np.array([i[0] for i in training_data]).reshape(-1,len(training_data[0][0]),1)
    y = [i[1] for i in training_data]
    
    if not model:
        #if model doesnt exist, we make new model
        model = neural_network_model(input_size= len(X[0]))
        
    model.fit({'input':X}, {"targets":y}, n_epoch=5,snapshot_step=500,show_metric=True, run_id="openAI")
    
    return model

training_data = initial_populations()
model = train_model(training_data)

#saving the model
model.save("cartpole.model")


#if we need to load model we use
#model.load("cartpole.model")

scores = []
choices = []

for each_game in range(100):
    score = 0
    game_memory = []
    prev_obs = []
    env.reset()
    for _ in range (goal_steps):
        #env.render()
        if len(prev_obs) == 0:
            action = random.randrange(0,2)
        else:
            action = np.argmax(model.predict(prev_obs.reshape(-1, len(prev_obs),1))[0])
            
        choices.append(action)
        
        new_observation, reward, done, info = env.step(action)
        prev_obs = new_observation
        game_memory.append([new_observation, action])
        score += reward
        if done: 
                break
    
    scores.append(score)
    

            
print("Average score:", sum(scores)/len(scores))
print("Choice 1: {}, Choice 0: {}".format(choices.count(1)/len(choices), choices.count(0)/len(choices)))
    